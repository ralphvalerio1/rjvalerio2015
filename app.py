from flask import Flask
from flask import render_template
from flask import request

app = Flask(__name__)

@app.route('/')
def hello_world():
	return 'Hello World!'

@app.route('/<username>', methods=['GET','POST'])
def hello_username(username):
	if request.method == 'POST':
		#request.form for post
		for x in request.form:
			print x + ' ' + str(request.form[x])
		return render_template('post.html',name = username)
	elif request.method == 'GET':
		#request.args for get
		for x in request.args:
			print x + ' ' + str(request.args[x])
		return render_template('hello.html',name = username)
	return 'Not supported'

@app.route('/<int:user_id>')
def hello_user_id(user_id):
	return 'User id: %d' %user_id

@app.route('/about')
def about():
	return 'this is about page'

if __name__ == '__main__':
	app.run()