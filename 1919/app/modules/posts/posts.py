from flask import Blueprint
from flask import render_template
from flask import g
from flask import request
from flask import redirect
from flask import url_for
from flask import flash
from flask import session

mod = Blueprint('posts', __name__)

@mod.route('/')
def item_list():
    items = g.postsdb.getItems(session['username'])
    carts = g.itemsdb.getCart(session['username'])
    return render_template('posts/post.html', items=items, carts = carts)

@mod.route('/', methods=['POST'])    
def add_item():
    item = request.form['item']
    price = request.form['price']
    if g.postsdb.checkItem(item).count() > 0:
        flash('Item already exists','add_item_failure')
        return redirect('/')
    g.postsdb.add_item(item, price, session['username'])
    flash('New item created!', 'create_item_success')
    return redirect(url_for('.item_list'))

@mod.route('/add_to_cart', methods = ['GET','POST'])
def add_to_cart():
	# quantity = request.form.getlist['quantity']
	items = g.postsdb.getItems(session['username'])
	carts = g.itemsdb.getCart(session['username'])
	if request.method == "POST":
		a = [x for x in request.form.getlist('quantity')]
		for x in xrange(0, len(a)):
			# if (carts <= 0):
				g.itemsdb.createCart(items[x]['item'], int(items[x]['price']) * int(a[x]),session['username'])
				
			# else:
			# 	g.itemsdb.updateCart(items[x]['item'], int(items[x]['price']) * int(a[x]))
				
	return redirect(url_for('.item_list'), items = items, carts = carts)

# @mod.route('/delete_cart', methods = ['GET'])
# def delete_cart():
# 	g.itemsdb.deleteCart(session['username'])
# 	return redirect(url_for('.item_list'))