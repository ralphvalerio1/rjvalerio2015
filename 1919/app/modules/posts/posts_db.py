# from bson.objectid import ObjectId

class PostDB:
    def __init__(self, conn):
        self.conn = conn

    def getPosts(self, username):
        return self.conn.find({'username': username})

    def createPost(self, post, username):
        self.conn.insert({'post':post, 'username': username})

    def getItems(self, username):
        return self.conn.find({'username': username})


    def checkItem(self, item):
        return self.conn.find({'item':item})

    def add_item(self, item, price, username):
        self.conn.insert({'item':item, 'price':price, 'username': username})


    # def updatePost(self,post,username,_id):
    # 	self.conn.update({'_id':ObjectId(_id), 'post':post, 'username':username})