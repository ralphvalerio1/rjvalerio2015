class ItemDB:
	def __init__(self, conn):
		self.conn = conn

	def getCart(self,username):
		return self.conn.find({'username':username})

	def createCart(self, item, price, username):
		self.conn.insert({'item':item, 'price': price, 'username':username})

	def updateCart(self,item,price):
		self.conn.update({'item':item, 'price': price})