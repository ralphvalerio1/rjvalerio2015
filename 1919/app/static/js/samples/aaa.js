var Post = React.createClass({
  render: function() {
    var style = {
      backgroundColor: this.props.bgcolor
    }
    return (
       <div style={style}>
         <b>Item name:</b> {this.props.author} <b>Price:</b> {this.props.children}
       </div>
    );
  }
});

var PostList = React.createClass({
  render: function() {
    var postNodes = this.props.data.map(function (comment, index) {
      return (
        <Post key={index} author={comment.author}>
          {comment.post}
        </Post>
      );
    });
    return (
      <div>
        {postNodes}
        // <input type="submit" value="Add Item to cart" />
      </div>
    );
  }
});

var PostForm = React.createClass({
  handleSubmit: function(e) {
    e.preventDefault();
    var form = e.target;
    var author = form.author.value.trim();
    var text = form.post.value.trim();
    if (!text || !author) {
      return;
    }
    this.props.onCommentSubmit({author: author, post: text});
    form.author.value = '';
    form.post.value = '';
    return;
  },
  handleSubmitCart: function(e) {
    e.preventDefault();
    var form = e.target;
    var author = form.author.value.trim();
    var text = form.post.value.trim();
    if (!text || !author) {
      return;
    }
    this.props.onCommentSubmitCart({author: author, post: text});
    form.author.value = '';
    form.post.value = '';
    return;
  },
  render: function() {
    return (
      <form onSubmit={this.handleSubmitCart}>
        <input type="submit" value="Add Item to Cart" />
      </form>
      <form onSubmit={this.handleSubmit}>
        <br/><br/>
        <input type="text" placeholder="Item Name" name="author"/>
        <br/>
        <input type="number" placeholder="Price" name="post"/>
        <br/>
        <input type="submit" value="Add Item" />
      </form>
    );
  }
});

var PostBox = React.createClass({
  loadCommentsFromServer: function() {
    $.ajax({
      url: this.props.url,
      dataType: 'json',
      cache: false,
      success: function(data) {
        var postDict = JSON.parse(JSON.stringify(data));   
        this.setState({data: postDict['posts']});
      }.bind(this),
      error: function(xhr, status, err) {
        console.error(this.props.url, status, err.toString());
      }.bind(this)
    });
  },
  handleCommentSubmit: function(comment) {
    $.ajax({
      url: this.props.url,
      dataType: 'json',
      type: 'POST',
      data: comment,
      success: function(data) {
        var postDict = JSON.parse(JSON.stringify(data));   
        this.setState({data: postDict['posts']});
      }.bind(this),
      error: function(xhr, status, err) {
        console.error(this.props.url, status, err.toString());
      }.bind(this)
    });
  },
  handleCommentSubmitCart: function(comment) {
    $.ajax({
      url: this.props.url,
      dataType: 'json',
      type: 'POST',
      data: comment,
      success: function(data) {
        var postDict = JSON.parse(JSON.stringify(data));   
        this.setState({data: postDict['posts']});
      }.bind(this),
      error: function(xhr, status, err) {
        console.error(this.props.url, status, err.toString());
      }.bind(this)
    });
  },
  getInitialState: function() {
    return {data: []};
  },
  componentDidMount: function() {
    this.loadCommentsFromServer();
    setInterval(this.loadCommentsFromServer, this.props.pollInterval);
  },
  render: function() {
    return (
      <div>
        <h3>Inventory</h3>
        <PostList data={this.state.data} />
        <PostForm onCommentSubmit={this.handleCommentSubmit} />
        <PostForm onCommentSubmitCart={this.handleCommentSubmitCart} />
      </div>
      // <div>
      //   <h3></h3>
      //   <CartPost data={this.state.data} />
      // </div>
    );
  }
});


// try cart
// var CartPost = React.createClass({
//   render: function() {
//     var style = {
//       backgroundColor: this.props.bgcolor
//     }
//     return (
//        <div style={style}>
//          <b>Item name:</b> {this.props.author} <b>Price:</b> {this.props.children}
//        </div>
//     );
//   }
// });


// var CartList = React.createClass({
//   render: function() {
//     var CartNodes = this.props.data.map(function (comment, index) {
//       return (
//         <CartPost key={index} author={comment.author}>
//           {comment.post}
//         </CartPost>
//         );
//     });
//     return (
//       <div>
//         {CartNodes}
//       </div>
//       );
//   }
// });